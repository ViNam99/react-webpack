import React from 'react';
import PropTypes from 'prop-types';

import Header from './Header';
import Footer from './Footer';

const DefaultLayout = ({ ...props }) => {
  return (
    <div className="default_layout">
      <Header />
      {props.children}
      <Footer />
    </div>
  );
};

export default DefaultLayout;
DefaultLayout.propTypes = {
  children: PropTypes.element,
};

DefaultLayout.defaultProps = {
  children: '',
};
