const initialState = {};

const AccountReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
export default AccountReducer;
