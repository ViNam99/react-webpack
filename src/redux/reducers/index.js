import { combineReducers } from 'redux';
import AccountReducer from './Account.reducer';
const rootReducer = combineReducers({
  AccountReducer,
});
export default rootReducer;
