import React from 'react';
import { NavLink } from 'react-router-dom';
const PageNotFound = () => {
  const prefix = 'pageNotFound';
  return (
    <>
      <section className={prefix}>
        <NavLink exact to="/">
          Back to Home
        </NavLink>
      </section>
    </>
  );
};

export default PageNotFound;
