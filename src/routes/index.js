import React from 'react';
import HomePage from '@/pages/HomePage';
import PageNotFound from '@/pages/PageNorFound';
import DefaultLayout from '@/layouts/DefaultLayout';

export const routes = [
  {
    path: '/',
    exact: true,
    main: () => (
      <DefaultLayout>
        <HomePage />
      </DefaultLayout>
    ),
  },
  {
    path: '',
    exact: false,
    main: () => <PageNotFound />,
  },
];
